
## Examen prácticode  PHP Customers - Orders Beitech SAS

RESTFul API fué desarrollado bajo la meotología TDD en el framework Laravel 8.

## Instalación

- Para correr el proyecto se requiere _PHP ^7.3_ en adelante.
- Instalar las dependencias.
```sh
composer install
```

- Hacer la siguiente configuración según base de datos.
```sh
// Cambia el nombre del archivo
~$ cp .env.example .env

// Editar las siguientes líneas del archivo .env
DB_CONNECTION=mysqñ
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

## Servicios API a invocar
- Url api en funcionamiento
```sh
https://apibeitech.dev-test.tech/
```

REST API - Usar el header HTTP “Accept: application/json” para realizar las peticiones.
```sh
Accept: application/json
```

| HOST | Method | URI | Desc | Parameter | Return |
| ------ | ------ |------ |------ |------ |------ |
| {HOST}/webapp/ | GET  | api/customer | Muestra todos los clientes |  | **List**<**Customer**> |
| {HOST}/webapp/ | GET  | api/orders | Filtra las ordenes por fecha y cliente | `?customer_id=@customer_id&date_from=@date_from&date_to=@date_to` | **List**<**OrdersDetails**> |
| {HOST}/webapp/ | POST | api/orders | Guarda las ordenes y los detalles de cada una |  | **Order** |
|
