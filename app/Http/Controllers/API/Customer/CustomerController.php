<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\BaseController;
use App\Models\Customer;

class CustomerController extends BaseController
{

    public function  index(){
        $data = Customer::all();
        return $this->sendResponse([
            'customer' => $data
        ], 'success', 200);
    }

    public function find_customer($customer_id){

        return Customer::where('customer_id',$customer_id)->exists();
    }
}
