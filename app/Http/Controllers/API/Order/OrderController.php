<?php

namespace App\Http\Controllers\API\Order;

use App\Http\Controllers\BaseController;
use App\Models\Order;
use App\Models\Product;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $this->validate($request,[
            'customer_id' => 'numeric',
            'date_from' => 'date_format:Y-m-d',
            'date_to' => 'date_format:Y-m-d',
        ]);
        $orders = Order::with('order_detail');

        if ($request->customer_id){
            $orders->where('customer_id',$request->customer_id);
        }
        if ($request->date_from && $request->date_to){
            $orders->whereBetween('creation_date',[$request->date_from,$request->date_to]);
        }elseif ($request->date_from){
            $orders->where('creation_date','>=',$request->date_from);
        }elseif ($request->date_to){
            $orders->where('creation_date','<=',$request->date_to);
        }

        $result = $orders->get();
        return $this->sendResponse(['orders' => $result], "List orders successfully.", 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $objOS = new OrderService();
        $validation = $this->validate($request,[
            'customer_id' => 'required|numeric',
            'delivery_address' => 'required',
            'products' => 'required|array',
            'total' => 'required',
            'products.*.product_id' => 'required|numeric',
            'products.*.quantity' => 'required|numeric|max:5',
        ]);

        //Validate products in order
        $orderValidator = $objOS->verifyProducts($validation["products"],$validation["customer_id"]);

        if(!$orderValidator[0]){
            return $this->sendError(['error'=>$orderValidator[1]], [], 422);
        }

        //Create Order
        $order = Order::create([
            'customer_id' => $request->customer_id,
            'creation_date' => Carbon::now()->format('Y-m-d'),
            'delivery_address' => $request->delivery_address,
            'total' => $request->total,
        ]);

        $totOrder = $objOS->registerOrder($validation["products"],$order->order_id);
        $udpOrder = Order::where('order_id',$order->order_id)->first();
        $udpOrder->total = $totOrder;
        $udpOrder->save();

        return $this->sendResponse(['order' => $udpOrder], "Order creates with successfully.", 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
