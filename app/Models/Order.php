<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = "order";
    protected $primaryKey = "order_id";
    public $timestamps = false;

    protected $fillable = [
        'customer_id',
        'creation_date',
        'delivery_address',
        'total',
        'date_from',
        'date_to',
    ];

    public function order_detail()
    {
        return $this->hasMany(OrderDetail::class,'order_id');
    }
    public function produtc()
    {
        return $this->hasMany(Product::class,'product_id');
    }
}
