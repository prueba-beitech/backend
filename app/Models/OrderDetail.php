<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class OrderDetail extends Model
{
    use HasFactory;

    protected $table = "order_detail";
    protected $primaryKey = "order_detail_id";

    public $timestamps = false;

    protected $fillable = [
        'order_id',
        'product_id',
        'product_description',
        'price',
        'quantity',
    ];

   /* public function orders(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Order::class);
    }*/

    public function productos()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}
