<?php

namespace App\Services;

use App\Models\CustomerProduct;
use App\Models\OrderDetail;
use App\Models\Product;

final class OrderService
{
    private const MAX_PRODUCTS_CUSTOMER = 5;

    public function verifyProducts($products,$customer_id){
        $quantity = 0;
        foreach ($products AS $product){
            if($this->ensureExistProduct($product['product_id'])){
                if(!$this->verifyCustomerProduct($product['product_id'],$customer_id)){
                    return [false, "Product does not belong to customer"];
                }
                $quantity = $quantity+$product['quantity'];
            }else{
                return [false, "Product entered does not exist"];
            }
        }
        if($quantity>self::MAX_PRODUCTS_CUSTOMER){
            return [false, "The quantity of products must be less than or equal to 5."];
        }
        return [true, "Product successfully validated"];
    }

    public function ensureExistProduct($product_id){
        return Product::where('product_id',$product_id)->exists();
    }

    public function verifyCustomerProduct($product_id,$customer_id){
        return CustomerProduct::where('product_id',$product_id)
            ->where('customer_id',$customer_id)
            ->exists();
    }

    public function registerOrder($products,$order_id){
        $total = 0;
        foreach ($products AS $product){
            $prd = Product::where('product_id',$product['product_id'])->first();
            $price = ($prd->price*$product['quantity']);
            $total = $total+$price;
            OrderDetail::create([
                "product_id"=>$product['product_id'],
                "order_id"=>$order_id,
                "product_description"=>$prd->product_description,
                "price"=>$price,
                "quantity"=>$product['quantity']
            ]);
        }
        return $total;
    }
}
