<?php

use App\Http\Controllers\API\Customer\CustomerController;
use App\Http\Controllers\API\Order\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/**
 * @OA\Info(title="API", version="0.0.1")
 **/

Route::middleware('api')->get('customer', [CustomerController::class, 'index']);
Route::resource('orders', OrderController::class);

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
