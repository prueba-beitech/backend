<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CustomerControllerTest extends TestCase
{

    public function testGetAllCustomers()
    {
        $this->getJson('api/customer')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' =>
                    [
                        'customer' =>[
                            '*' => [
                                'customer_id',
                                'name',
                                'email'
                            ]
                        ]
                    ],
                'message' => 'success',
            ]);
    }


}
