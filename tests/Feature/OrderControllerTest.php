<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class OrderControllerTest extends TestCase
{
    public function testRequiredFieldForRegistrationOrder(){
        $this->postJson('api/orders')
            ->assertStatus(422)
            ->assertJson([
               "message" => "The given data was invalid.",
               "errors"=>[
                   "customer_id" => ["The customer id field is required."],
                   "delivery_address" => ["The delivery address field is required."],
                   "products" => ["The products field is required."],
                   "total" => ["The total field is required."],
               ]
            ]);
    }

    public function  testInvalidNumberProducts(){

        $data = [
            'customer_id' => 1,
            'total' => 1,
            'delivery_address' => 'Calle falsa 123',
            'products' => [
                [
                    'product_id'=>10,
                    "quantity"=>4
                ],
                [
                    'product_id'=>15,
                    "quantity"=>3
                ]
            ],
        ];

        $this->postJson( 'api/orders', $data)
            ->assertStatus(422)
            ->assertJsonStructure([
                "message" => [
                    "error",
                ]
            ]);

    }

    public function testOrderCreatedSuccessfully()
    {
        $data = [
            'customer_id' => 1,
            'total' => 1,
            'delivery_address' => 'Calle falsa 123',
            'products' => [
                [
                    'product_id'=>10,
                    "quantity"=>1
                ],
                [
                    'product_id'=>15,
                    "quantity"=>3
                ]
            ],
        ];

        $this->postJson( 'api/orders', $data)
            ->assertStatus(JsonResponse::HTTP_CREATED)
            ->assertJsonStructure([
                "data" => [
                    "order" => [
                        'order_id',
                        'customer_id',
                        'creation_date',
                        'delivery_address',
                        'total',
                    ]
                ],
                "message"
            ]);
    }

}
