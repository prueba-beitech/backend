<?php

namespace Tests\Unit;

use App\Http\Controllers\API\Customer\CustomerController;
use Tests\TestCase;

class CustomerControllerTest extends TestCase
{

    public function testCustomerDoesNotExist()
    {
        $customer_id= 1000;
        $customer = new CustomerController();
        $response = $customer->find_customer($customer_id);

        $this->assertFalse($response);
    }

    public function testCustomerExist()
    {
        $customer_id= 1;
        $customer = new CustomerController();
        $response = $customer->find_customer($customer_id);

        $this->assertTrue($response);
    }
}
