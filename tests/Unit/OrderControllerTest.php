<?php

namespace Tests\Unit;

use App\Services\OrderService;
use Tests\TestCase;
class OrderControllerTest extends TestCase
{
    public function testProductDoesNotExist(){
        $orderVal = new OrderService();
        $ressult = $orderVal->ensureExistProduct(3000);
        $this->assertFalse($ressult);
    }

    public function testProductExist(){
        $product = new OrderService();
        $ressult = $product->ensureExistProduct(1);
        $this->assertTrue($ressult);
    }

    public function testProductBelongsCustomer(){
        $orderVal = new OrderService();
        $ressult = $orderVal->verifyCustomerProduct(10,1);
        $this->assertTrue($ressult);
    }

    public function testProductDoesNotBelongCustomer(){
        $orderVal = new OrderService();
        $ressult = $orderVal->verifyCustomerProduct(1,1);
        $this->assertFalse($ressult);
    }
}
